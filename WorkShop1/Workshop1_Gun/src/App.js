import profileImage from "./profile.jfif";
import "./App.css";
function App() {
  return (
    <>
      <div className="head">
        <img className="profile" src={profileImage} />
        <h1>ชื่อ นามสกุล</h1>
      </div>
      <div className="content">
        <p>ชื่อ: นาย ปวิธ อึ้งอารุณยะวี</p>
        <p>ชื่อเล่น: กัน</p>
        <p>ตำแหน่ง: Front end</p>
        <p>Skill:</p>
        <ul>
          <li>html</li>
          <li>css</li>
        </ul>
        <p>งานอดิเรก:</p>
        <ul>
          <li>นอน</li>
          <li>เล่นเกม</li>
          <li>นอน</li>
        </ul>
      </div>
    </>
  );
}

export default App;
